import { Container, Row, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { css } from 'glamor'
export const BgHeader = (props) => {
    const header = css({
        backgroundPosition: 'center',
        backgroundImage: `url('${props.background}')`,
        backgroundRepeat: `no - repeat`,
        backgroundSize: `cover`,
        height: `100vh`
    })
    css.insert(`
    @import url('https://fonts.googleapis.com/css2?family=Quicksand&display=swap');
    @font-face {
        font-family: 'photograph_signatureregular';
        src: url('/font/photograph-signature-webfont.woff2') format('woff2'),
             url('/font/photograph-signature-webfont.woff') format('woff');
        font-weight: normal;
        font-style: normal;
    
    }
    .row {
        color:#D0B577;
    }
    p { font-size: 0.9em;
        width:100%;
        text-align:center;
        font-family: 'Quicksand', sans-serif;
        margin-bottom:0
    }
    h1{
        font-family:'photograph_signatureregular';
        font-weight:400;
        font-size:3.3em;
    }
    i{
        letter-spacing: -2px;
    }
    button.btn-light{
        background-color:#D0B577;
        border-color:#D0B577;
        border-radius:15px;
        padding-left:30px;
        padding-right:30px
    }
    button p{
        color:white;
    }
    

    `)
    return (
        <Container fluid className={`${header} position-relative`}>
            <Row
                className="w-100 justify-content-center"
                style={{
                    position: 'absolute',
                    bottom: '10%'
                }}>
                {props.children}
            </Row>
        </Container>
    )
}
