import React, { Component } from 'react'
import { BgHeader, IsiHeader, Logo } from 'template/general/Header'
import { Button } from 'react-bootstrap'

let id = 'mella-cahyo'
let inisial_co = 'Cahyo'
let inisial_ce = 'Mella'

let lengkap_co = (<>Cahyo Rizki Dwiokta,ST  </>)
let lengkap_ce = (<>Wahyu Mella Harita,ST </>)

let bapak_co = 'Bpk Sudrajat'
let ibu_co = 'Ibu Sri Hartuti'
let bapak_ce = "Bpk Chamdi Aryandi"
let ibu_ce = "Ibu Sri Sunarsih"

let ig_co = "cahyo_rizki"
let ig_ce = "mellaharita"
export default class Gold1 extends Component {
    render() {
        return (
            <>
                <BgHeader background="https://di.undang.in/amel-danar/modal.jpg">
                    <p>THE WEDDING OF :</p>
                    <h1>{inisial_ce} & {inisial_co}</h1>
                    <p>
                        <i className="pr-2">------------</i>
                        21.02.2021
                        <i className="pl-2">------------</i>
                    </p>
                    <Button variant="light mt-3">
                        <p>Open Invitiation</p>
                    </Button>
                </BgHeader>
            </>
        )
    }

}